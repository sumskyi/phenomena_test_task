require 'spec_helper'

describe LanguagesController do

  describe "GET 'search'" do
    it "returns http success" do
      get 'search', :format => :json, :q => 'abc'
      response.should be_success
    end
  end

end
