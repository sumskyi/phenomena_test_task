require 'spec_helper'

describe CampaignsController do
  describe "index" do
    it "success" do
      get :index
      response.should be_success
    end
  end


  describe "create" do
    it "creates a new Campaign" do
      Campaign.any_instance.should_receive(:save)
      post :create
    end
  end

  context 'concrete campaign' do
    let(:campaign) { mock('campaign') }
    before { controller.should_receive(:campaign).and_return(campaign) }

    describe "update" do
      it "updates the requested campaign" do
        campaign.should_receive(:update_attributes).and_return(true)
        put :update, :id => 2345678
      end
    end

    describe "destroy" do
      it "destroys the requested campaign" do
        campaign.should_receive(:destroy)
        delete :destroy, :id => 9876543
      end
    end
  end
end
