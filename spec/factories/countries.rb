# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country do
    short "MyString"
    name "MyString"
  end
end
