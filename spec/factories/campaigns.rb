# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :campaign do
    starts_at { 1.day.from_now }
    ends_at { 1.week.from_now }
  end
end
