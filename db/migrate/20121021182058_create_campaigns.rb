class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.datetime :starts_at
      t.datetime :ends_at
      t.text :countries_languages
    end
  end
end
