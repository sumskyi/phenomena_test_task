@javascript

Feature: create the Campaign

Background:
  Given I am on the campaigns page
  And countries on file
  And languages on file

Scenario: campaigns list
  When I follow "New Campaign"
  Then I fill in the following:
    | starts_at | 01-10-2012 |
    | ends_at   | 31-12-2012 |
  And I follow "Add country"
  And I select "Albania" from "countries_languages"
  And I fill in "token-input-" with "Albanian"
  And press "Create Campaign"
  Then I should see "Albania"
  Then a campaign should exist

