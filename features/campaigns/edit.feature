@javascript

Feature: edit the Campaign

Background:
  Given countries on file
  And languages on file
  And a campaigns on file

Scenario: campaigns list
  Given I am on the campaigns page
  When I follow "Edit"
  And I select "Albania" from "countries_languages"
  And I fill in "token-input-" with "Albanian"
  And press "Update Campaign"
  Then I should see "Albania"
  And a campaign should exist


