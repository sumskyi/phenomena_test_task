PhenomenaTestTask::Application.routes.draw do
  match "countries" => "countries#index"
  match "languages/search" => "languages#search"

  resources :campaigns

  root :to => 'campaigns#index'
end
