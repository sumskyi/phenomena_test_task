class Campaign < ActiveRecord::Base
  attr_accessible :starts_at, :ends_at, :countries_languages

  class JSONSerializer
    def load text
      ActiveSupport::JSON.decode text, :symbolize_keys => true
    rescue MultiJson::DecodeError
      []
    end

    def dump text
      ActiveSupport::JSON.encode text
    end
  end

  serialize :countries_languages, JSONSerializer.new

  validates :starts_at, :ends_at, :presence => true
end
