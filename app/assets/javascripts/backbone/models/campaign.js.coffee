class PhenomenaTestTask.Models.Campaign extends Backbone.Model
  paramRoot: 'campaign'

  defaults:
    starts_at: null
    ends_at: null
    countries_languages: null

class PhenomenaTestTask.Collections.CampaignsCollection extends Backbone.Collection
  model: PhenomenaTestTask.Models.Campaign
  url: '/campaigns'
