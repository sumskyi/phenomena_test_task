class PhenomenaTestTask.Models.Language extends Backbone.Model
  paramRoot: 'language'

class PhenomenaTestTask.Collections.LanguagesCollection extends Backbone.Collection
  model: PhenomenaTestTask.Models.Language
  url: '/languages'

