#= require 'backbone/views/campaigns/persistence_view'

PhenomenaTestTask.Views.Campaigns ||= {}

class PhenomenaTestTask.Views.Campaigns.EditView extends PhenomenaTestTask.Views.Campaigns.PersistenceView
  template : JST["backbone/templates/campaigns/edit"]

  events :
    "submit #edit-campaign" : "update"
    "click #add-country": "add_row"

  constructor: (options) ->
    super(options)
    @model.on("change:errors", @renderErrors, this)

  update : (e) ->
    e.preventDefault()
    e.stopPropagation()

    @fill_inputs()

    @model.unset("errors")

    @model.save(null,
      success : (campaign) =>
        @model = campaign
        window.location.hash = "/#{@model.id}"

      error: (campaign, jqXHR) =>
        @model.set({errors: $.parseJSON(jqXHR.responseText)})
    )



  add_row: (model) ->
    view = new PhenomenaTestTask.Views.Campaigns.CountryRowView({model: model})
    table = this.$el.find('#countries')
    table.prepend(view.render().el)
    return

  add_rows: ->
    collection = new PhenomenaTestTask.Collections.CountriesLanguagesCollection @model.get('countries_languages')

    self = @
    collection.each (cl) -> self.add_row(cl)
    return
