PhenomenaTestTask.Views.Countries ||= {}

class PhenomenaTestTask.Views.Countries.OptionView extends Backbone.View
  template: JST["backbone/templates/countries/option"]

  tagName: 'select'

  initialize: (options) ->
    @current_country = options.current_country
    @collection = new PhenomenaTestTask.Collections.CountriesCollection()

  render: ->
    template = @template
    current_country = @current_country
    el = $(@el)
    el.attr('name', 'countries_languages');
    @collection.fetch
      success: (data) ->
        el.html(template(countries: data.toJSON(), current_country: current_country))
        return

    return this


