class LanguagesController < ApplicationController
  respond_to :json

  expose(:search_languages) { Language.where(["name LIKE ?", "%#{params[:q]}%"]) }

  def search
    respond_with search_languages
  end
end
